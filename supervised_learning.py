# supervised learning for ECS 271 homework 1

# for reading CSV data:
import csv

#for math:
import math
import numpy
from scipy.stats import mode

#for cross-validation:
from sklearn import cross_validation

train_file = 'pendigits-train.csv'
test_file = 'pendigits-test-nolabels.csv'

#######################
# K-Nearest Neighbors #
#######################

#value of k for KNN:
k = 10

class training_instance:
    def __init__(self, pairs, value):
        self.pairs = pairs
        self.value = value

class dist_pair:
    def __init__(self, distance, value):
        self.distance = distance
        self.value = value

# function to calculate 8-dimensional distance
def distance(train_array, test_array):
    distances = [0]*8
    
    for idx, pair in enumerate(train_array):
        distances[idx] = ((pair[0] - pair[0])**2 + (pair[1] - pair[1])**2)**0.5

    sum = 0
    for dist in distances:
        sum += dist**2

    return sum**0.5

def knn(X_train, X_test, y_train):
#for each instance in test data, find k nearest training instances; output predicted value
    for i, instance in enumerate(X_test):
        dists = []
        for idx, train_instance in enumerate(X_train):
            dists.append( dist_pair(distance(train_instance, instance), y_train[idx]))

        sorted_dists = sorted(dists, key=lambda dist_pair: dist_pair.distance)
        sorted_dists = sorted_dists[0:k]
        vals = []
        for idx, entry in enumerate(sorted_dists):
            vals.append(sorted_dists[idx].value)

        prediction = mode(vals)

        print(prediction)


# read in the data here. each pair = x-y coordinate pair.

with open(train_file, 'r') as csvfile:
    #numline = len(csvfile.readlines())
    #print(numline)
    training_data = []
    training_pairs = []
    training_values = []*10
    
    reader = csv.reader(csvfile, delimiter=',')
    
    for row in reader:
        instance = []
        instance.append( (int(row[0]), int(row[1])) )
        instance.append( (int(row[2]), int(row[3])) )
        instance.append( (int(row[4]), int(row[5])) )
        instance.append( (int(row[6]), int(row[7])) )
        instance.append( (int(row[8]), int(row[9])) )
        instance.append( (int(row[10]), int(row[11])) )
        instance.append( (int(row[12]), int(row[13])) )
        instance.append( (int(row[14]), int(row[15])) )
        
        entry = training_instance
        entry.pairs = instance
        entry.value = int(row[16])
        
        training_data.append(entry)
        
        training_pairs.append(instance)
        training_values.append(int(row[16]))


#    for i, inst in enumerate(training_data):
#       print(training_data[i].pairs)


# for each instance in the training set,
# - find k nearest (labeled) neighbors;
# - find label (is it better to average the nearest labels, or to take whichever has the most instances within the neighborhood?)

#with open(test_file, 'r') as csvfile:
#    reader = csv.reader(csvfile, delimiter=',')

#cross-validation:
X_train, X_test, y_train, y_test = cross_validation.train_test_split(
    training_pairs, training_values, test_size=0.4, random_state=0)

knn(X_train, X_test, y_train)






